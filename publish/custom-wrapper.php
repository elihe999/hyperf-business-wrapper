<?php

declare(strict_types=1);

return [
    'response' => [
        'success' => [
            'code',
            'message',
            'data'
        ],
        'failed' => [
            'code',
            'message'
        ]
    ]
];