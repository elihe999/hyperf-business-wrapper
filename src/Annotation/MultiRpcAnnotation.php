<?php

namespace Blockgolde\HyperfBusinessWrapper\Annotation;

use Hyperf\RpcServer\Annotation\RpcService;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 *
 * Shorthand for RpcService(name="hyperf-skeleton", protocol="jsonrpc", server="jsonrpc-tcp", publishTo="consul")
 */
class MultiRpcAnnotation extends AbstractAnnotation
{
    /**
     * @var string
     */
    public string $name = '';

    /**
     * @var string
     */
    public $protocol = 'jsonrpc';

    /**
     * @var string
     */
    public $server = 'jsonrpc-tcp';

    /**
     * The counter required to reset to a open state.
     * @var string
     */
    public $publishTo = 'consul';

    public function __construct(...$value)
    {
        parent::__construct(...$value);
    }

    public function collectClass(string $className): void
    {
        $rpc_server = new RpcService();
        $rpc_server->name = $this->name;
        $rpc_server->protocol = $this->protocol;
        $rpc_server->server = $this->server;
        $rpc_server->publishTo = env('RPC_PROTOCOL') ?: $this->publishTo;
        AnnotationCollector::collectClass($className, RpcService::class, $rpc_server);
    }
}