<?php

declare(strict_types=1);

namespace Blockgolde\HyperfBusinessWrapper\Exception;

use Throwable;
use Hyperf\Server\Exception\ServerException;
use Blockgolde\HyperfBusinessWrapper\Constants\ErrorCode;

class ThrottleRequestsException extends ServerException
{

    public function __construct(int $code = 0, string $message = null, Throwable $previous = null)
    {
        if (is_null($message)) {
            $message = ErrorCode::getMessage($code);
        }

        parent::__construct($message, $code, $previous);
    }

}