<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Blockgolde\HyperfBusinessWrapper\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class ErrorCode extends AbstractConstants
{

    /**
     * @Message("Success")
     */
    public const SUCCESS = 200;

    /**
     * @Message("服务器错误")
     */
    public const INTERNAL_ERROR = 500;

    /**
     * @Message("资源不存在")
     */
    public const ERR_NON_EXISTENT = 404;

    /**
     * @Message("请求异常")
     */
    public const ERROR = 400;

    /**
     * @Message("登录已过期，请重新登录")
     */
    public const ERR_HTTP_UNAUTHORIZED = 401;

    /**
     * @Message("无权操作")
     */
    public const NO_AUTH = 403;

    /**
     * @Message("不允许请求该方法")
     */
    public const ERR_HTTP_METHOD_NOT_ALLOWED = 405;

    /**
     * @Message("请求体类型错误")
     */
    public const ERR_HTTP_UNSUPPORTED_MEDIA_TYPE = 415;

    /**
     * @Message("参数校验错误")
     */
    public const ERR_HTTP_UNPROCESSABLE_ENTITY = 422;

    /**
     * @Message("请求频次达到上限")
     */
    public const REQUEST_FREQUENTLY = 429;
    /**
     * @Message("方法不存在")
     */
    public const METHOD_NOT_ALLOWED = 1000;
    /**
     * @Message("Server Error！")
     */
    public const SERVER_ERROR = 9999;

    /**
     * @Message("需要登陆")
     */
    public const ERR_NOT_LOGIN = 58001;

    /**
     * @Message("用户鉴权失败")
     */
    public const HEADER_ERROR = 58005;

    /**
     * @Message("上传配置有误")
     */
    public const UPDATE_PARAM_ERROR = 52010;

    /**
     * @Message("上传图片失败")
     */
    public const UPDATE_IMAGE_FAILED = 52011;

    /**
     * @Message("参数验证失败")
     */
    public const ERR_VALIDATE_FAILED = 58100;

    /**
     * @Message("Excel参数错误")
     */
    public const PARAMETER_ERROR = 54007;

    /**
     * @Message("导入文件失败")
     */
    public const FAILED_TO_IMPORT_FILES_PROCEDURE = 54008;

    /**
     * @Message("配置解析失败")
     */
    public const FAILED_TO_EXTRACT = 54009;

    /**
     * @Message("请选择对应的对应操作符")
     */
    public const ERR_INVALID_FILTER_CONDITION = 59301;

    /**
     * @Message("选中的数据类型不支持Like Not-Like")
     */
    public const ERR_INVALID_OPERATION_LIKE = 59302;

    /**
     * @Message("选中的数据类型需要传入数字类型的值")
     */
    public const ERR_INVALID_NUMBER_VALUE = 59303;
    /**
     * @Message("选中的日期类型的值为非法格式")
     */
    public const ERR_INVALID_DATE_FORMAT = 59304;
    /**
     * @Message("数据查询异常")
     */
    public const ERR_INVALID_GET_DATA = 59310;

    /**
     * @Message("不支持的SQL操作")
     */
    public const ERR_INVALID_SQL_ACTION = 54110;

    /**
     * @Message("参数缺失")
     */
    public const PARAM_MISSING = 10000;

    /**
     * @Message("参数错误")
     */
    public const PARAM_ERROR = 10001;

    /**
     * @Message("数据库操作失败")
     */
    public const ERR_QUERY = 10002;

    /**
     * @Message("数据库连接失败")
     */
    public const ERR_DB = 10003;

    /**
     * @Message("数据不存在")
     */
    public const ERR_MODEL = 10004;
}
