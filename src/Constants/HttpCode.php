<?php

declare(strict_types=1);

namespace Blockgolde\HyperfBusinessWrapper\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class HttpCode extends AbstractConstants
{
    /**
     * @Message("Continue")
     * 继续。客户端应继续其请求
     */
    const CONTINUE = 100;

    /**
     * @Message("Switching Protocols")
     * 切换协议。服务器根据客户端的请求切换协议。只能切换到更高级的协议，例如，切换到HTTP的新版本协议
     */
    const SWITCH_PROTOCOLS = 100;

    /**
     * @Message("OK")
     * 对成功的 GET、PUT、PATCH 或 DELETE 操作进行响应。也可以被用在不创建新资源的 POST 操作上
     */
    const OK = 200;

    /**
     * @Message("Created")
     * 对创建新资源的 POST 操作进行响应。应该带着指向新资源地址的 Location 头
     */
    const CREATED = 201;

    /**
     * @Message("Accepted")
     * 服务器接受了请求，但是还未处理，响应中应该包含相应的指示信息，告诉客户端该去哪里查询关于本次请求的信息
     */
    const ACCEPTED = 202;

    /**
     * @Message("Non-Authoritative Information")
     * 非授权信息。请求成功。但返回的meta信息不在原始的服务器，而是一个副本
     */
    const NOT_AUTH_INFO = 203;

    /**
     * @Message("No Content")
     * 无内容。服务器成功处理，但未返回内容。在未更新网页的情况下，可确保浏览器继续显示当前文档
     */
    const NO_CONTENT = 204;

    /**
     * @Message("Reset Content")
     * 重置内容。服务器处理成功，用户终端（例如：浏览器）应重置文档视图。可通过此返回码清除浏览器的表单域
     */
    const RESET_CONTENT = 205;

    /**
     * @Message("Partial Content")
     * 部分内容。服务器成功处理了部分GET请求
     */
    const RARTIAL_CONTENT = 206;

    /**
     * @Message("Multiple Choices")
     * 多种选择。请求的资源可包括多个位置，相应可返回一个资源特征与地址的列表用于用户终端
     */
    const MULTIPLE_CHOICES = 300;

    /**
     * @Message("Moved Permanently")
     * 被请求的资源已永久移动到新位置
     */
    const MOVED_PERMANENTLY = 301;

    /**
     * @Message("Found")
     * 请求的资源现在临时从不同的 URI 响应请求
     */
    const FOUNT = 302;

    /**
     * @Message("See Other")
     * 对应当前请求的响应可以在另一个 URI 上被找到，客户端应该使用 GET 方法进行请求。比如在创建已经被创建的资源时，可以返回 303
     */
    const SEE_OTHER = 303;

    /**
     * @Message("Not Modified")
     * HTTP缓存header生效的时候用
     */
    const NOT_MODIFIED = 304;

    /**
     * @Message("Use Proxy")
     * HTTP缓存header生效的时候用
     */
    const USE_PROXY = 305;

    /**
     * @Message("Unused")
     * HTTP缓存header生效的时候用
     */
    const UNUSED = 306;

    /**
     * @Message("Temporary Redirect")
     * 对应当前请求的响应可以在另一个 URI 上被找到，客户端应该保持原有的请求方法进行请求
     */
    const TEMPORARY_REDIRECT = 307;

    /**
     * @Message("Bad Request")
     * 请求异常，比如请求中的body无法解析
     */
    const BAD_REQUEST = 400;

    /**
     * @Message("Unauthorized")
     * 没有进行认证或者认证非法
     */
    const UNAUTHORIZED = 401;

    /**
     * @Message("Payment Required")
     * 保留
     */
    const PAYMENT_REQUIRED = 402;

    /**
     * @Message("Forbidden")
     * 服务器已经理解请求，但是拒绝执行它
     */
    const FORBIDDEN = 403;

    /**
     * @Message("Not Found")
     * 请求一个不存在的资源
     */
    const NOT_FOUND = 404;

    /**
     * @Message("Method Not Allowed")
     * 所请求的 HTTP 方法不允许当前认证用户访问
     */
    const METHOD_NOT_ALLOWED = 405;

    /**
     * @Message("Not Acceptable")
     * 所请求的 HTTP 方法不允许当前认证用户访问
     */
    const NOT_ACCEPTABLE = 406;

    /**
     * @Message("Proxy Authentication Required")
     * 所请求的 HTTP 方法不允许当前认证用户访问
     */
    const PROXY_AUTH_REQUIRED = 407;

    /**
     * @Message("Request Time-out")
     * 所请求的 HTTP 方法不允许当前认证用户访问
     */
    const REQUEST_TIMEOUT = 408;

    /**
     * @Message("Conflict")
     * 所请求的 HTTP 方法不允许当前认证用户访问
     */
    const CONFLICT = 409;
    /**
     * @Message("Gone")
     * 表示当前请求的资源不再可用。当调用老版本 API 的时候很有用
     */
    const GONE = 410;

    /**
     * @Message("Length Required")
     * 请求的URI过长（URI通常为网址），服务器无法处理
     */
    const LENGTH_REQUIRED = 411;

    /**
     * @Message("Precondition Failed")
     * 请求的URI过长（URI通常为网址），服务器无法处理
     */
    const PRECONDITION_FAILED = 412;
    /**
     * @Message("Request Entity Too Large")
     * 请求的URI过长（URI通常为网址），服务器无法处理
     */
    const REQUEST_ENTITY_TOO_LARGE = 413;

    /**
     * @Message("Request-URI Too Large")
     * 请求的URI过长（URI通常为网址），服务器无法处理
     */
    const REQUEST_URL_TOO_LARGE = 414;
    
    /**
    * @Message("Requested range not satisfiable")
    * 请求的URI过长（URI通常为网址），服务器无法处理
    */
   const REQUEST_RANGE_NOT_SATISFIABLE = 416;

    /**
     * @Message("Unsupported Media Type")
     * 如果请求中的内容类型是错误的
     */
    const UNSUPPORTED_MEDIA_TYPE = 415;

    /**
     * @Message("Expectation Failed")
     * 服务器无法满足Expect的请求头信息
     */
    const EXPECTATION_FAILED = 417;

    /**
     * @Message("Unprocessable Entity")
     * 用来表示校验错误
     */
    const UNPROCESSABLE_ENTITY = 422;

    /**
     * @Message("Too Many Requests")
     * 由于请求频次达到上限而被拒绝访问
     */
    const TOO_MANY_REQUESTS = 429;

    /**
     * @Message("Internal Server Error")
     * 服务器遇到了一个未曾预料的状况，导致了它无法完成对请求的处理
     */
    const SERVER_ERROR = 500;

    /**
     * @Message("Not Implemented")
     * 服务器不支持当前请求所需要的某个功能
     */
    const NOT_IMPLEMENTED = 501;

    /**
     * @Message("Bad Gateway")
     * 作为网关或者代理工作的服务器尝试执行请求时，从上游服务器接收到无效的响应
     */
    const BAD_GATEWAY = 502;

    /**
     * @Message("Service Unavailable")
     * 由于临时的服务器维护或者过载，服务器当前无法处理请求。这个状况是临时的，并且将在一段时间以后恢复。如果能够预计延迟时间，那么响应中可以包含一个 Retry-After
     * 头用以标明这个延迟时间（内容可以为数字，单位为秒；或者是一个 HTTP 协议指定的时间格式）。如果没有给出这个 Retry-After 信息，那么客户端应当以处理 500 响应的方式处理它
     */
    const SERVICE_UNAVAILABLE = 503;

    /**
     * @Message("Gateway Timeout")
     */
    const GATEWAY_TIMEOUT = 504;

    /**
     * @Message("HTTP Version Not Supported")
     */
    const HTTP_VERSION_NOT_SUPPORTED = 505;

    /**
     * @Message("Variant Also Negotiates")
     */
    const VARIANT_ALSO_NEGOTIATES = 506;

    /**
     * @Message("Insufficient Storage")
     */
    const INSUFFICIENT_STORAGE = 507;

    /**
     * @Message("Loop Detected")
     */
    const LOOP_DETECTED = 508;

    /**
     * @Message("Not Extended")
     */
    const NOT_EXTENDED = 510;

    /**
     * @Message("Network Authentication Required")
     */
    const NETWORK_AUTHENTICATION_REQUIRED = 511;

    /**
     * @Message("Network Connect Timeout Error")
     */
    const NETWORK_CONNECT_TIMEOUT_ERROR = 599;
}