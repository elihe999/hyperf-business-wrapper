<?php
namespace Blockgolde\HyperfBusinessWrapper\Utils;

class PaginationParamHelper
{
    public $nowPageKey = 'page';
    public $pageSizeKey = 'pageSize';
    public function solvePageParam(array $params): array
    {
        if (isset($params[$this->nowPageKey]) && !empty($params[$this->nowPageKey])) {
            $nowPage = $params[$this->nowPageKey];
        } else {
            $params[$this->nowPageKey] = 1;
        }
        if (isset($params[$this->pageSizeKey]) && !empty($params[$this->pageSizeKey])) {
            $perPage = $params[$this->pageSizeKey];
        } else {
            $params[$this->pageSizeKey] = 10;
        }
        return $params;
    }

    public function getPageKey(): string
    {
        return $this->nowPageKey;
    }

    public function getPageSizeKey(): string
    {
        return $this->pageSizeKey;
    }
}