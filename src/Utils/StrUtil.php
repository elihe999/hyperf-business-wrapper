<?php

namespace Blockgolde\HyperfBusinessWrapper\Utils;

use Hyperf\Utils\Str;

class StrUtil extends Str
{
    function check_mobile($mobile): bool
    {
        if (preg_match("/^1[3|4|5|7|8|9]{1}[0-9]{9}$/", $mobile))
            return true;
        return false;
    }

    /**
     * @param       $haystack
     * @param array $needles
     *
     * @return bool
     *
     * 是否包含在给定的多个字符串中的一个
     */
    public static function containsIn($haystack, array $needles): bool
    {
        foreach ($needles as $needle) {
            if (static::contains($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    public static function byteLength(string $string): int
    {
        return mb_strlen($string, '8bit');
    }

    /**
     * @param $columnIndex
     *
     * @return mixed|string
     *
     * 数字转字符
     */
    public static function stringFromColumnIndex($columnIndex)
    {
        static $indexCache = [];

        if (!isset($indexCache[$columnIndex])) {
            $indexValue = $columnIndex;
            $base26 = null;
            do {
                $characterValue = ($indexValue % 26) ?: 26;
                $indexValue = ($indexValue - $characterValue) / 26;
                $base26 = chr($characterValue + 64) . ($base26 ?: '');
            } while ($indexValue > 0);
            $indexCache[$columnIndex] = $base26;
        }

        return $indexCache[$columnIndex];
    }

}