<?php

namespace Blockgolde\HyperfBusinessWrapper\Utils;

use Throwable;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Logger\LoggerFactory;

class LogUtil
{
    const LOG_LEVEL = [
        'DEBUG' => 'debug',
        'INFO' => 'info',
        'NOTICE' => 'notice',
        'WARNING' => 'warning',
        'ERROR' => 'error',
        'CRITICAL' => 'critical',
        'ALERT' => 'alert',
        'EMERGENCY' => 'emergency',
    ];

    const MSG_LEVEL = [
        'OTHER' => 0,
        'QUERY' => 1,
        'ADD' => 2,
        'EDIT' => 3,
        'DEL' => 4,
        'NORMAL' => 5,
        'ERROR' => 6,
    ];

    /**
     * @param string $message 日志消息
     * @param int $type 日志类型:1,错误日志,2运行日志,3系统日志,4,统计日志
     * @param int $code 日志状态码
     * @param string $uri 前端路由
     * @param string $other 其他字段
     * @param string $err_file 日志地址(三方请求IP)
     * @param int $level 日志级别
     * @return void
     */
    public static function addLog(string $message, string $group_name = 'default', string $name = 'app', $type = 'info', array $contexts)
    {
        ApplicationContext::getContainer()->get(LoggerFactory::class)->get($group_name, $name)->$type($message, $contexts);
    }

    public static function get(string $group_name = 'default', string $name = 'app')
    {
        try {
            return ApplicationContext::getContainer()->get(LoggerFactory::class)->get($name, $group_name);
        } catch (Throwable $e) {
            return ApplicationContext::getContainer()->get(LoggerFactory::class)->get('app', 'default');
        }
    }
}
